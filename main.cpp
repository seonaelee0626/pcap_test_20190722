#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#define ETHERH_SIZE 14
#define MAC_LEN 6
#define IP_LEN 4
#define PORT_LEN 2
#define PRINT_DATA_SIZE 10
#define WORD_SIZE 4

// ethernet header : 14B
// 1. smac, dmac
// 2. type field : network layer - ip protocol or not?
struct ethernet_header{
    u_int8_t dmac[MAC_LEN];
    u_int8_t smac[MAC_LEN];
    u_int16_t type;
};

// ip header
// 1. sip, dip
// 2. ihl field : length of ip header - tcp starting offset?
struct ip_header{
    u_int8_t ver_ipHdrLen;           // 0xf0 & ver_ipHdrLen = ver, 0x0f & ver_ipHdrLen = ipHdrLen
    u_int8_t service;
    u_int16_t total_len;
    u_int16_t id;
    u_int16_t flags_fragOffset;
    u_int8_t ttl;
    u_int8_t proto;
    u_int16_t checksum;
    u_int32_t sip;
    u_int32_t dip;
    char *options;
};

// tcp header
// 1. sport, dport
// 2. Hlen field : length of tcp header - tcp payload staring offset?
struct tcp_header{
    u_int16_t sport;
    u_int16_t dport;
    u_int32_t seq_num;
    u_int32_t ack_num;
    u_int16_t hlen_reserved_flags;
    u_int16_t window;
    u_int16_t checksum;
    u_int16_t urgent_ptr;
    char *options;
};

void print_mac(ethernet_header *eth_hdr);
void print_ip(ip_header *ip_hdr);
void print_tcp_port(tcp_header *tcp_hdr);
void print_tcp_data(u_char *data, int size);

void usage() {
  printf("syntax: pcap_test <interface>\n");
  printf("sample: pcap_test wlan0\n");
}

int main(int argc, char* argv[]) {
  if (argc != 2) {
    usage();
    return -1;
  }

  char* dev = argv[1];
  char errbuf[PCAP_ERRBUF_SIZE];
  //pcap_open_live(device to open, size, promiscuous=1,
  pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);

//  pcap_t* handle = pcap_open_offline("/home/seonaelee/Downloads/test/test.pcapng",errbuf);
  if (handle == NULL) {
    fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
    return -1;
  }

  while (true) {
    struct pcap_pkthdr* header;
    const u_char* packet;
    int res = pcap_next_ex(handle, &header, &packet);
    //matching packet to ethernet_header struct type
    ethernet_header *ethHdr = (ethernet_header*)packet;
    print_mac(ethHdr);

    // if the packet is on IP protocol, print src/dest IP address
    // type field of Ethernet protocol = 0x0800 -> IP protocol
    // little endian : 0x0800 => 0x0008
    if(ethHdr->type==0x0008){
        // IP header format starts after ethernet header
        ip_header *ipHdr = (ip_header *)(packet+ETHERH_SIZE);
        print_ip(ipHdr);

        // if the packet is on TCP protocol, print TCP src/dest port.
        if(ipHdr->proto==0x06){
            // tcp header starts after IP header.
            // IP header size = ipHdrLen field * 4
            int ip_hdr_size = (u_int8_t)(((ipHdr->ver_ipHdrLen)&0x0f)*WORD_SIZE);
            tcp_header *tcpHdr = (tcp_header *)(packet+ETHERH_SIZE+ip_hdr_size);
            print_tcp_port(tcpHdr);
            // tcp data size = total packet size - ethernet header size - ip header size - tcp header size
            int tcp_data_size = ((header->caplen)-ETHERH_SIZE-(u_int8_t)(((ipHdr->ver_ipHdrLen)&0x0f)*WORD_SIZE)-(u_int8_t)((((tcpHdr->hlen_reserved_flags)&0x00f0)>>4)*WORD_SIZE));
            if(tcp_data_size > 0) // if tcp data is above 0, print tcp data.
                print_tcp_data((u_char*)packet+(header->caplen-tcp_data_size), tcp_data_size);
        }
    }

    if (res == 0) continue;
    if (res == -1 || res == -2) break;
    printf("%u bytes captured\n", header->caplen);
  }

  pcap_close(handle);
  return 0;
}

// print src/dest mac address
void print_mac(ethernet_header *eth_hdr){
    printf("dmac = %02x:%02x:%02x:%02x:%02x:%02x\n", eth_hdr->dmac[0],eth_hdr->dmac[1],eth_hdr->dmac[2],eth_hdr->dmac[3],eth_hdr->dmac[4],eth_hdr->dmac[5]);
    printf("smac = %02x:%02x:%02x:%02x:%02x:%02x\n", eth_hdr->smac[6],eth_hdr->dmac[7],eth_hdr->dmac[8],eth_hdr->dmac[9],eth_hdr->dmac[10],eth_hdr->dmac[11]);
}

// print src/dest ip address
void print_ip(ip_header *ip_hdr){
    printf("dip = %d.%d.%d.%d\n", (0x000000ff & ip_hdr->dip), (0x0000ff00 & ip_hdr->dip)>>8,(0x00ff0000 & ip_hdr->dip)>>16,(0xff000000 & ip_hdr->dip)>>24);
    printf("sip = %d.%d.%d.%d\n", (0x000000ff & ip_hdr->sip), (0x0000ff00 & ip_hdr->sip)>>8,(0x00ff0000 & ip_hdr->sip)>>16,(0xff000000 & ip_hdr->sip)>>24);
}

// print tcp src/dest port address
void print_tcp_port(tcp_header *tcp_hdr){
    printf("dport = %d\n", (u_int16_t)(((tcp_hdr->dport & 0xff)<<8)+((tcp_hdr->dport & 0xff00)>>8)));
    printf("sport = %d\n", (u_int16_t)(((tcp_hdr->sport & 0xff)<<8)+((tcp_hdr->sport & 0xff00)>>8)));
}

// print tcp data
void print_tcp_data(u_char *data, int size){
    printf("tcp data = ");
    for(int i = 0; (i < PRINT_DATA_SIZE) && (i < size); i++){
        printf("\\0x%02x", data[i]);
    }
    printf("\n");
}
